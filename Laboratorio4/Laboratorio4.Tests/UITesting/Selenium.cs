﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
namespace UnitTestLab7.UITesting
{
    [TestClass]
    public class Selenium
    {
        IWebDriver driver;
        [TestMethod]
        public void PruebaListadoPlanetas()
        {
            ///Arrange
            /// Crea el driver de Chrome
            /// Si se deja el default chrome en versiones actuales explota
            driver = new ChromeDriver(@"Ruta a chrome driver");
            string URL = "http://localhost:49393";
            /// Pone la pantalla en full screen
            driver.Manage().Window.Maximize();
            ///Act
            /// Se va a la URL indicada
            driver.Url = URL;
            
            IWebElement enlaceListarPlanetas = driver.FindElement(By.Id("test"));
            enlaceListarPlanetas.Click();
            IWebElement h1 = driver.FindElement(By.Id("tituloTest"));

            ///Assert
            Assert.AreEqual("Listado de planetas", h1.Text);
        }

        [TestMethod]
        public void PruebaPrimerPlanetaValores()
        {
            ///Arrange
            /// Crea el driver de Chrome
            driver = new ChromeDriver(@"C:\Users\Luis\Downloads");
            string URL = "http://localhost:49393";
            /// Pone la pantalla en full screen
            driver.Manage().Window.Maximize();
            ///Act
            /// Se va a la URL indicada
            driver.Url = URL;

            IWebElement enlaceListarPlanetas = driver.FindElement(By.Id("test"));
            enlaceListarPlanetas.Click();
            IWebElement nombrePlaneta = driver.FindElement(By.Id("nombre1"));
            IWebElement tipoPlaneta = driver.FindElement(By.Id("tipo1"));
            IWebElement anillosPlaneta = driver.FindElement(By.Id("anillos1"));

            ///Assert
            Assert.AreEqual("Nombre: Tierra", nombrePlaneta.Text);
            Assert.AreEqual("Tipo: Rocoso", tipoPlaneta.Text);
            Assert.AreEqual("Anillos: 0", anillosPlaneta.Text);
        }

        [TestCleanup]
        public void TearDown()
        {
            if (driver != null)
            {
                driver.Quit();
            }
        }
    }
}